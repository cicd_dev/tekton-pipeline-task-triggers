k apply -f rbac.yaml
k delete -f rbac.yaml
k apply -f gitlab-secret.yaml
k delete -f gitlab-secret.yaml
k apply -f gitlab-trigger-pipeline-listener.yaml
k delete -f gitlab-trigger-pipeline-listener.yaml

k apply -f trigger-ingress.yaml
k delete -f trigger-ingress.yaml
