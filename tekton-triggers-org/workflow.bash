
kubectl apply -f hello-world.yaml
kubectl apply -f hello-world-run.yaml
kubectl apply --filename goodbye-world.yaml
kubectl apply --filename hello-goodbye-pipeline.yaml
kubectl apply --filename hello-goodbye-pipeline-run.yaml
kubectl apply -f trigger-template.yaml
kubectl apply -f trigger-binding.yaml
kubectl apply -f rbac.yaml
kubectl apply -f event-listener.yaml

kubectl port-forward service/el-hello-listener 8080

curl -v \
   -H 'content-Type: application/json' \
   -d '{"username": "Tekton"}' \
   http://localhost:8080